# Command line tools for encryption and decryption
[![Crates.io](https://img.shields.io/crates/v/fus.svg)](https://crates.io/crates/mook)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/mook)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/mook/-/raw/master/LICENSE)
```
Command line tools for encryption and decryption

Usage: mook <COMMAND>

Commands:
  ef    encrypt_file
  df    decrypt_file
  et    decrypt_text
  dt    decrypt_text
  help  Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help information
  -V, --version  Print version information
```
