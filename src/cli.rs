#[allow(warnings)]
pub mod crypt{
    pub fn encrypt(data:Vec<u8>,password:&str)->Vec<u8>{
        let cryptor = tindercrypt::cryptors::RingCryptor::new();        
        
        match cryptor.seal_with_passphrase(password.as_bytes(), &data) {
            Ok(r)=>{r},
            Err(e)=>{data}
        }
    }
    pub fn decrypt(data:Vec<u8>,password:&str)->Vec<u8>{
        let cryptor = tindercrypt::cryptors::RingCryptor::new();
        
        match cryptor.open(password.as_bytes(), &data){
            Ok(r)=>{r},
            Err(e)=>{data}
        }
    }
}
#[allow(warnings)]
pub mod cli {
    use clap::{arg, Arg, ArgAction, Command};

    use crate::cli::crypt::encrypt;
    use crate::cli::crypt::decrypt;

    pub fn cli() {
        let matches = Command::new("mook")
            .about("Command line tools for encryption and decryption")
            .version("1.0.0")
            .subcommand_required(true)
            .arg_required_else_help(true)
            .allow_external_subcommands(true)
            .author("Anonymous")
            .subcommand(
                Command::new("ef")
                    .about("encrypt_file")
                    .arg(arg!([path] "the file path"))
                    .arg(
                        Arg::new("password")
                            .long("password")
                            .short('p')
                            .required(true)
                            .action(clap::ArgAction::Append),
                    )
                    .arg_required_else_help(true),
            )
            .subcommand(
                Command::new("df")
                    .about("decrypt_file")
                    .arg(arg!([path] "the file path"))
                    .arg(
                        Arg::new("password")
                            .long("password")
                            .short('p')
                            .required(true)
                            .action(clap::ArgAction::Append),
                    )
                    .arg_required_else_help(true),
            )
            .subcommand(
                Command::new("et")
                    .about("decrypt_text")
                    .arg(arg!([input] "The String text to be encrypted"))
                    .arg(
                        Arg::new("password")
                            .long("password")
                            .short('p')
                            .required(true)
                            .action(clap::ArgAction::Append),
                    )
                    .arg_required_else_help(true),
            )
            .subcommand(
                Command::new("dt")
                    .about("decrypt_text")
                    .arg(arg!([input] "The String text to be decrypted"))
                    .arg(
                        Arg::new("password")
                            .long("password")
                            .short('p')
                            .required(true)
                            .action(clap::ArgAction::Append),
                    )
                    .arg_required_else_help(true),
            )
            .get_matches();
        match matches.subcommand_name() {
            Some("ef") => {
                let path = if let Some(dt) = matches.subcommand_matches("ef") {
                    let text = dt.get_one::<String>("path").unwrap();
                    text
                } else {
                    ""
                };

                let password = if let Some(dt) = matches.subcommand_matches("ef") {
                    let pass = dt
                        .get_one::<String>("password")
                        .map(|s| s.as_str())
                        .unwrap();
                    pass
                } else {
                    "zaqxsw"
                };
                let file = std::fs::read(path).unwrap();
                let encrypted_file = encrypt(file, password);
                std::fs::write(path, encrypted_file).unwrap();
            }
            Some("df") => {
                let path = if let Some(dt) = matches.subcommand_matches("df") {
                    let text = dt.get_one::<String>("path").unwrap();
                    text
                } else {
                    ""
                };

                let password = if let Some(dt) = matches.subcommand_matches("df") {
                    let pass = dt
                        .get_one::<String>("password")
                        .map(|s| s.as_str())
                        .unwrap();
                    pass
                } else {
                    "zaqxsw"
                };
                let file = std::fs::read(path).unwrap();
                let decrypted_file = decrypt(file, password);
                std::fs::write(path, decrypted_file).unwrap();
            }
            Some("et") => {
                let text = if let Some(dt) = matches.subcommand_matches("et") {
                    let text = dt.get_one::<String>("input").unwrap();
                    text
                } else {
                    ""
                };

                let password = if let Some(dt) = matches.subcommand_matches("et") {
                    let pass = dt
                        .get_one::<String>("password")
                        .map(|s| s.as_str())
                        .unwrap();
                    pass
                } else {
                    "zaqxsw"
                };
                let encrypted_text = encrypt(text.as_bytes().to_vec(), password);
                let encrypted_text_string = encrypted_text.into_iter().map(|s|s.to_string()).collect::<Vec<String>>().join(":");
                println!("{}",encrypted_text_string);
            }
            Some("dt") => {
                let text = if let Some(dt) = matches.subcommand_matches("dt") {
                    let text = dt.get_one::<String>("input").unwrap();
                    text
                } else {
                    ""
                };

                let password = if let Some(dt) = matches.subcommand_matches("dt") {
                    let pass = dt
                        .get_one::<String>("password")
                        .map(|s| s.as_str())
                        .unwrap();
                    pass
                } else {
                    "zaqxsw"
                };
                let text_vec = text.split(":").filter(|s|!s.is_empty()).map(|s|s.parse::<u8>().unwrap()).collect::<Vec<u8>>();
                let decrypted_text = decrypt(text_vec, password);
                let decrypted_text_string = String::from_utf8_lossy(&decrypted_text).to_string();
                println!("{}",decrypted_text_string);
            }
            _ => {}
        }
    }
    
}
